#ifndef BASE_ORDER
#define BASE_ORDER

#include "OrderBookEvent.h"

enum OrderStatus{NEW,REPLACED,PARTIALLY_FILLED,FILLED,CANCELLED};

const OrderStatus openSet[]={NEW,REPLACED,PARTIALLY_FILLED};//11 states

class Order{
	public:
		Order();
		~Order();

		void ProcessOrderBookEvent(const OrderBookEventPtr& value);

		void SetPrice(double value);
		void SetFilledPrice(double value);
		void SetSize(double value);
		void SetFilledSize(double value);
		void SetID(long long value);
		void SetDirection(long long value);
		void SetOrderStatus(OrderStatus value);
		void SetCurrentLevel(int value);

		double GetPrice() const;
		double GetFilledPrice() const;
		double GetSize() const;
		double GetFilledSize() const;
		long long GetID() const;
		long GetDirection() const;
		int GetCurrentLevel() const;

		double	GetWorkingSize() const;
		double	GetCreatedTime() const;
		double	GetModifiedTime() const;
		double	GetLastestTime() const;
		bool	IsModified() const;

		OrderStatus GetOrderStatus() const;

		bool IsOpen() const;
	private:
		float px;
		float filledPx;
		float sz;
		float filledSz;
		long long id;
		int direction;
		bool isModified;
		int currentLvl;

		float createdTime,modifiedTime,lastestTime;
		OrderStatus ordStatus;

		std::vector<OrderBookEventPtr> events;
};

typedef boost::shared_ptr<Order> OrderPtr;
#endif