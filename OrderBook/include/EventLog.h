#ifndef EVENT_LOG_H__
#define EVENT_LOG_H__
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/tee.hpp>
#include <boost/shared_ptr.hpp>
#include <fstream>
#include <iostream>

typedef boost::iostreams::tee_device<std::ostream, std::ofstream> Tee;
typedef boost::iostreams::stream<Tee> TeeStream;
typedef boost::iostreams::stream< boost::iostreams::null_sink >  NullStream;

enum LOG_LEVEL{LOG_INFO,LOG_WARN,LOG_FATAL,LOG_DEBUG, FIX_LOG};

class Logger{	
public:
	static void SetPath(std::string Path);
	static void SetConsoleLogging(bool value);
	static TeeStream& Log(LOG_LEVEL level = LOG_INFO);
	static std::ofstream& LogToFile(LOG_LEVEL level = LOG_INFO);
private:
	static std::ofstream* filePtr;
	static TeeStream* streamPtr;
	static NullStream* nullStreamPtr;
	static bool consoleOuput;
};

#endif //__LOG_H__
