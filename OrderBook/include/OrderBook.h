#ifndef TRAXEX_CORE_ORDERBOOK_H
#define TRAXEX_CORE_ORDERBOOK_H
#include <utility>
#include "OrderBookEvent.h"
#include "Order.h"
#include "OrderBookSnapshot.h"
class OrderBook {
	public:
		OrderBook(long minPrice, long maxPrice, std::vector<long> pxVec, long bookLevel);
		
		void SetAutoMatchOrders(bool value);
		bool IsAutoMatchOrders() const;

		virtual std::vector<OrderBookSnapshotPtr>	AddEvent(const OrderBookEventPtr& evt);
		std::vector<OrderPtr> GetOrders();
		virtual ~OrderBook();
	private:
		OrderBook(const OrderBook &);


		friend class OrderBookSnapshot;

		bool isAutoMatchOrders;
		std::vector<OrderPtr> activeOrderList;
		std::vector< std::vector<OrderPtr> > bidDepth,askDepth;
		std::vector< double > bidSize, askSize;
		std::vector<OrderPtr> orderCollection;
		std::vector<long> pxVec;
		OrderBookEventPtr lastEvent;
		long maxPx, minPx, bookLevel;
};

typedef boost::shared_ptr<OrderBook> OrderBookPtr;

#endif