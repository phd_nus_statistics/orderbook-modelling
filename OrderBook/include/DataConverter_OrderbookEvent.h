#ifndef ORDERBOOK_DATA_CONVERTER
#define ORDERBOOK_DATA_CONVERTER

#include "OrderBookEvent.h"
#include <vector>
#include "boost/shared_ptr.hpp"

class DataConverter_OrderbookEvent{
	public:
		DataConverter_OrderbookEvent();
		~DataConverter_OrderbookEvent();

		std::vector<OrderBookEventPtr> GetOrderBookEvents() const;
		void ProcessFile(const std::string& filePath, bool isReset=true);		
	protected:
		virtual void OnProcessFile(const std::string& filePath)=0;
		virtual void OnNewEventRaw(char* data, int len)=0;
		std::vector<OrderBookEventPtr> orderbookEvents;		
};

typedef boost::shared_ptr<DataConverter_OrderbookEvent> DataConverter_OrderbookEvent_Ptr;
#endif