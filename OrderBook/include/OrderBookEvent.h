#ifndef __OrderBookEvent__
#define __OrderBookEvent__
#include "boost/date_time/posix_time/posix_time.hpp"
#include "boost/shared_ptr.hpp"

enum class OrderEventType { Add, Delete, Change };
enum class OrderEventReason {Submission, Cancellation, Deletion, Execution_Visible_Limit, Execution_Hidden_Limit, TradingHalt};

class DataFactory;
class OrderBookEvent {
	public:		
		OrderBookEvent(long long id, OrderEventType type, OrderEventReason rs, double px, double vol, int direction,double origTime, long pos=-1);		
		
		//Setter
		void SetOrderEventType(OrderEventType value);
		void SetOrderEventReason(OrderEventReason value);
		void SetPrice(double value);
		void SetSize(double value);
		void SetDirection(long value);
		void SetID(long long value);
		void SetOriginalEventTime(double value);
		void SetHitBookLevel(long value);
		
		//Getter
		OrderEventType GetOrderEventType() const;
		OrderEventReason GetOrderEventReason() const;
		double GetPrice() const;
		double GetSize() const;
		long GetDirection() const;
		long long GetID() const;
		double GetOriginalEventTime() const;
		long GetHitBookLevel() const;
	private:
		friend class DataFactory;
		OrderBookEvent();
		OrderBookEvent(OrderBookEvent &);

		OrderEventType eventType;
		OrderEventReason reason;
		float price, size;
		long direction;
		long long id;
		float originalTime;
		long hitBookLevel;
};

typedef boost::shared_ptr<OrderBookEvent> OrderBookEventPtr;

#endif