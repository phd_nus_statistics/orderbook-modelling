#ifndef DATA_CONVERTER_LOBSTER
#define DATA_CONVERTER_LOBSTER
#include "DataConverter_OrderbookEvent.h"

enum TimeResolution{Millisecond, Nanosecond};

class DataConverter_OrderbookEvent_LOBSTER:public DataConverter_OrderbookEvent{
	public:
		DataConverter_OrderbookEvent_LOBSTER(long tickSz);
		~DataConverter_OrderbookEvent_LOBSTER();
	protected:
		virtual void OnNewEventRaw(char* data, int len);
		virtual void OnProcessFile(const std::string& filePath);
	private:
		boost::posix_time::ptime startTime;
		std::vector<std::string> timeSplitStr;
		boost::posix_time::ptime evtTime;
		OrderEventType evtType;
		OrderEventReason evtReason;
		long long orderID;
		long long orderSize;
		long long orderPx;
		int direction;
		double originalTime;
		long tickSize;
};

typedef boost::shared_ptr<DataConverter_OrderbookEvent_LOBSTER> DataConverter_OrderbookEvent_LOBSTER_Ptr;
#endif

