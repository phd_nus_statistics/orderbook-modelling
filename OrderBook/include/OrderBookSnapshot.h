#ifndef TRAXEX_CORE_ORDERBOOK_SNAPSHOT_H
#define TRAXEX_CORE_ORDERBOOK_SNAPSHOT_H
#include "OrderBookEvent.h"
#include "boost/date_time.hpp"
#include <vector>

class OrderBook;

#define SNAPSHOT_LEVEL 5
struct SnapshotData{
	double px;
	double sz;
	double avgOrdAgeInMs;
	double noModifed;
	double noOrd;
	OrderBookEventPtr lastEvent;

	SnapshotData():px(0),sz(0),avgOrdAgeInMs(0),noModifed(0),noOrd(0){}
};
struct OrderBookSnapshot{
	public:
		OrderBookSnapshot(const OrderBook* ordBook);

		std::vector<SnapshotData> snapshotData_BID,snapshotData_ASK;
		double snapshotTime;
		bool isLastTradeBid;
		bool isLastTradeAsk;
};

typedef boost::shared_ptr<OrderBookSnapshot> OrderBookSnapshotPtr;

#endif