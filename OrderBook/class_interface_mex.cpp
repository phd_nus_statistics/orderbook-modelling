#include "mex.h"
#include "matrix.h"
#include <string>
#include <sstream>
#include <set>
#include <algorithm>
#include "boost/date_time.hpp"
#include "DataConverter_OrderbookEvent_LOBSTER.h"
#include "OrderBook.h"
#include "OrderBookSnapshot.h"
#include "EventLog.h"
static boost::posix_time::ptime epoch= boost::posix_time::ptime(boost::gregorian::date(1970,01,01),boost::posix_time::time_duration(0,0,0));
static boost::posix_time::ptime maxTime= boost::posix_time::ptime(boost::gregorian::date(2200,01,01),boost::posix_time::time_duration(0,0,0));\

std::string MexCharArrayToString(const mxArray *plhs){
	//Convert MATLAB mxArray to std string
	char* buf;
	int buflen;
	int n = mxGetN(plhs);
	buflen = n*sizeof(mxChar) + 1;
	buf = (char*)mxMalloc(buflen);
	mxGetString(plhs, buf, buflen);
	std::stringstream ss;
	for (int i = 0; i < n; i++){
		ss << buf[i];
	}
	mxFree(buf);
	return ss.str();
}
long long					GetMillisecondsSinceEpoch(boost::posix_time::ptime t1){
	//Convert boost posix_time to milliseconds since epoch
	if (t1==boost::posix_time::max_date_time){
		return LLONG_MAX;
	}else if (t1==boost::posix_time::min_date_time){
		return 0;
	}else{
		if (t1<epoch) t1=epoch;
		else if (t1>maxTime) t1=maxTime;
		boost::posix_time::time_duration const diff = t1 - epoch;
		long long ms = diff.total_milliseconds();
		return ms;			// with nanosecond resolution
	}
}
boost::posix_time::ptime	ParseFromMillisecondsSinceEpoch(const long long& noMs){
	//Convert milliseconds since epoch to boost posix_time
	if (noMs>=LLONG_MAX){
		return boost::posix_time::max_date_time;
	}else if (noMs==0){
		return boost::posix_time::min_date_time;
	}else{
		boost::posix_time::time_duration t =boost::posix_time::milliseconds(noMs);
		return epoch+t;
	}
}
double BoostDateTimeToMATLABDatenum(const boost::posix_time::ptime& value){
	//Boost posix_time to MATLAB datenum format 
	return 719529.0 + (double)GetMillisecondsSinceEpoch(value)/8.64e7;;
}
boost::posix_time::ptime MATLABDatenumToBoostDateTime(double value){
	//MATLAB datenum format to boost posix_time
	return ParseFromMillisecondsSinceEpoch((value-719529.0)*8.64e7);
}


static std::map<long long,std::vector<OrderBookSnapshotPtr>> orderbookSnapshotMap	= std::map<long long,std::vector<OrderBookSnapshotPtr>> ();
static std::map<long long, std::vector<OrderBookEventPtr>> orderbookEventMap		= std::map<long long, std::vector<OrderBookEventPtr>>();
static std::map<long long, OrderBookPtr> orderbookMap								= std::map<long long, OrderBookPtr>();

static long long count=0;
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){	   
	auto command=MexCharArrayToString(prhs[0]);
	//Set to true to output log to a file
	Logger::SetConsoleLogging(false);
	if (!command.compare("SetPath")) {
		auto fileName = MexCharArrayToString(prhs[1]);
		Logger::SetPath(fileName);
	}else if (!command.compare("SetConsoleLogging")) {
		long long isConsoleLoggingOn = *((long long*)mxGetData(prhs[1]));

		Logger::SetConsoleLogging((bool)isConsoleLoggingOn);
	}else if (!command.compare("LoadEventsFromLOBSTER")) {
		auto fileName = MexCharArrayToString(prhs[1]);
		long tickSz = *((double*)mxGetData(prhs[2]));

		DataConverter_OrderbookEvent_LOBSTER dataConverter(tickSz);
		
		dataConverter.ProcessFile(fileName);
		auto events = dataConverter.GetOrderBookEvents();
				
		plhs[0] = mxCreateNumericMatrix(1, 1, mxINT64_CLASS, mxREAL);
		nlhs = 1;

		count++;
		orderbookEventMap[count] = events;
		((long long*)mxGetData(plhs[0]))[0] = count;
	}if (!command.compare("LoadEvents")) {
		double *data = mxGetPr(prhs[1]);
		mwSize mrows = mxGetM(prhs[1]);
		mwSize ncols = mxGetN(prhs[1]);

		//Load pre-processed events from LoadEventsFromLOBSTER
		std::vector<OrderBookEventPtr> events;
		for (auto mm = 0; mm<mrows; mm++) {		
			auto evtTime = data[0 * (mrows)+mm];
			auto evtType = data[1 * (mrows)+mm];
			auto evtID   = data[2 * (mrows)+mm];
			auto evtSz   = data[3 * (mrows)+mm];
			auto evtPx	 = data[4 * (mrows)+mm];
			auto evtDir  = data[5 * (mrows)+mm];

			OrderEventType evtNativeType = OrderEventType::Add;
			OrderEventReason evtReason = OrderEventReason::Submission;
			if (evtType == 3) {
				evtNativeType = OrderEventType::Delete;
				evtReason = OrderEventReason::Deletion;
			}
			OrderBookEventPtr evt(new OrderBookEvent(evtID, evtNativeType, evtReason, evtPx, evtSz, evtDir, evtTime));

			events.push_back(evt);
		}

		plhs[0] = mxCreateNumericMatrix(1, 1, mxINT64_CLASS, mxREAL);
		nlhs = 1;

		count++;
		orderbookEventMap[count] = events;
		((long long*)mxGetData(plhs[0]))[0] = count;		
	}
	else if (!command.compare("ClearEvents")) {
		long long dataKey = *((long long*)mxGetData(prhs[1]));
		orderbookEventMap.erase(dataKey);
	}
	else if (!command.compare("ClearBook")) {
		long long dataKey = *((long long*)mxGetData(prhs[1]));
		orderbookMap.erase(dataKey);
		orderbookSnapshotMap.erase(dataKey);
	}
	else if (!command.compare("DiffEventTimes")) {
		long long dataKey_1 = *((long long*)mxGetData(prhs[1]));
		long long dataKey_2 = *((long long*)mxGetData(prhs[2]));
		std::vector<OrderBookSnapshotPtr> diff_events;

		std::set_difference(orderbookSnapshotMap[dataKey_1].begin(), orderbookSnapshotMap[dataKey_1].end(), orderbookSnapshotMap[dataKey_2].begin(), orderbookSnapshotMap[dataKey_2].end(),
			std::inserter(diff_events, diff_events.begin()), [](const OrderBookSnapshotPtr& v1, const OrderBookSnapshotPtr& v2){
			return v1->isLastTradeAsk < v2->isLastTradeAsk;
		});
		plhs[0] = mxCreateDoubleMatrix(diff_events.size(), 1, mxREAL);
		nlhs = 1;

		for (auto index = 0; index < diff_events.size(); index++) {
			((double*)mxGetData(plhs[0]))[index] = diff_events[index]->snapshotTime;
		}
	}
	else if (!command.compare("ProcessOrderbookEvents")) {
		long long dataKey		= *((long long*)mxGetData(prhs[1]));
		long bookLevel = *((double*)mxGetData(prhs[2]));
		double isAutoMatch	= *((double*)mxGetData(prhs[3]));
		long minPx = *((double*)mxGetData(prhs[4]));
		long maxPx = *((double*)mxGetData(prhs[5]));

		auto events = orderbookEventMap[dataKey];

		std::vector<long> pxVec;
		for (auto iter = events.begin(); iter != events.end();) {
			long cPx = (*iter)->GetPrice();
			if (cPx == 0 || (cPx > minPx && cPx < maxPx)){
				iter++;
			}else{
				iter = events.erase(iter);
			}
		}		
		for (auto px = minPx; px <= maxPx; px++){
			pxVec.push_back(px);
		}

		OrderBookPtr orderbook(new OrderBook(minPx, maxPx, pxVec, bookLevel));
		orderbookMap[count]= orderbook;
		orderbook->SetAutoMatchOrders(isAutoMatch == 1);

		
		std::vector<OrderBookSnapshotPtr> snapshots;
		for (auto evtIdx=0;evtIdx<events.size();evtIdx++){
			auto generatedSnapshots=orderbook->AddEvent(events[evtIdx]);
			snapshots.insert(snapshots.end(), generatedSnapshots.begin(), generatedSnapshots.end());
		}
		orderbookSnapshotMap[count]=snapshots;
		plhs[0] = mxCreateNumericMatrix(1, 1, mxINT64_CLASS, mxREAL);		
		plhs[1] = mxCreateDoubleMatrix(events.size(), 8, mxREAL);	

		((long long*)mxGetData(plhs[0]))[0]=count;
		for (auto dataIdx=0;dataIdx<events.size();dataIdx++){
			for (auto subIdx=0;subIdx<8;subIdx++){
				mwSize indicies[2]={dataIdx,subIdx};
				size_t index = mxCalcSingleSubscript(plhs[1], 2, indicies); 

				double retVal=0;
				switch (subIdx){
					case 0:	retVal=events[dataIdx]->GetOriginalEventTime();	break;
					case 1:
						switch (events[dataIdx]->GetOrderEventReason()) {//Re-encode into LOBSTER data format
							case OrderEventReason::Submission:					retVal = 1;	break; 
							case OrderEventReason::Cancellation:				retVal = 2;	break;
							case OrderEventReason::Deletion:					retVal = 3;	break;
							case OrderEventReason::Execution_Visible_Limit:		retVal = 4;	break;
							case OrderEventReason::Execution_Hidden_Limit:		retVal = 5;	break;
							case OrderEventReason::TradingHalt:					retVal = 7;	break;
						}
					break;
					case 2:	retVal=events[dataIdx]->GetID();									break;
					case 3:	retVal=events[dataIdx]->GetSize();									break;
					case 4:	retVal=events[dataIdx]->GetPrice();									break;
					case 5:	retVal=events[dataIdx]->GetDirection();								break;
					case 6:	retVal=events[dataIdx]->GetHitBookLevel();							break;
					case 7:	retVal = static_cast<int>(events[dataIdx]->GetOrderEventType());	break;
				}
				((double*)mxGetData(plhs[1]))[index] = retVal;
			}
		}

		nlhs=1;
		count++;
	}else if (!command.compare("ExtractDataFromSnapshots")){
		auto subCommand=MexCharArrayToString(prhs[1]);
		long long dataKey	= *((long long*)mxGetData(prhs[2])); 
		long noLevel	= *((double*)mxGetData(prhs[3])); 
		if (orderbookSnapshotMap.find(dataKey)!=orderbookSnapshotMap.end()){
			auto data=orderbookSnapshotMap[dataKey];

			plhs[0] = mxCreateDoubleMatrix(data.size(), 1, mxREAL);			
			plhs[1] = mxCreateDoubleMatrix(data.size(), noLevel,  mxREAL);			
			plhs[2] = mxCreateDoubleMatrix(data.size(), noLevel,  mxREAL);			
			
			
			for (auto dataIdx=0;dataIdx<orderbookSnapshotMap[dataKey].size();dataIdx++){
				((double*)mxGetData(plhs[0]))[dataIdx] = data[dataIdx]->snapshotTime;
				
				for (auto subIdx=0;subIdx<noLevel;subIdx++){					
					mwSize indicies[2]={dataIdx,subIdx};
					size_t index = mxCalcSingleSubscript(plhs[1], 2, indicies); 

					if (subIdx<data[dataIdx]->snapshotData_BID.size()){

						if (!subCommand.compare("Price")){
							((double*)mxGetData(plhs[1]))[index] = data[dataIdx]->snapshotData_BID[subIdx].px;	
						}else if (!subCommand.compare("Size")){
							((double*)mxGetData(plhs[1]))[index] = data[dataIdx]->snapshotData_BID[subIdx].sz;	
						}
					}else{
						((double*)mxGetData(plhs[1]))[index] = 0;	
					}
					if (subIdx<data[dataIdx]->snapshotData_ASK.size()){
						if (!subCommand.compare("Price")){
							((double*)mxGetData(plhs[2]))[index] = data[dataIdx]->snapshotData_ASK[subIdx].px;	
						}else if (!subCommand.compare("Size")){
							((double*)mxGetData(plhs[2]))[index] = data[dataIdx]->snapshotData_ASK[subIdx].sz;	
						}
					}else{
						((double*)mxGetData(plhs[2]))[index] = 0;	
					}
				}
			}
			
			nlhs = 3;
		}
			
	}else if (!command.compare("ExtractTradeSignalFromSnapshots")){
		long long dataKey	= *((long long*)mxGetData(prhs[1]));
		if (orderbookSnapshotMap.find(dataKey)!=orderbookSnapshotMap.end()){
			auto data=orderbookSnapshotMap[dataKey];

			plhs[0] = mxCreateDoubleMatrix(data.size(), 1, mxREAL);			
			plhs[1] = mxCreateDoubleMatrix(data.size(), 1,  mxREAL);			
			plhs[2] = mxCreateDoubleMatrix(data.size(), 1,  mxREAL);			
			
			
			for (auto dataIdx = 0; dataIdx<data.size(); dataIdx++){
				((double*)mxGetData(plhs[0]))[dataIdx] = data[dataIdx]->snapshotTime;
				((double*)mxGetData(plhs[1]))[dataIdx] = (data[dataIdx]->isLastTradeBid)?(1):(0);
				((double*)mxGetData(plhs[2]))[dataIdx] = (data[dataIdx]->isLastTradeAsk)?(1):(0);				
			}
			
			nlhs = 3;
		}
	}else if (!command.compare("ExtractOrdersFromOrderbook")) {
		long long dataKey = *((long long*)mxGetData(prhs[1]));
		if (orderbookMap.find(dataKey) != orderbookMap.end()) {
			auto data = orderbookMap[dataKey]->GetOrders();

			plhs[0] = mxCreateDoubleMatrix(data.size(), 8, mxREAL);

			for (auto dataIdx = 0; dataIdx<data.size(); dataIdx++) {
				OrderPtr ord= data[dataIdx];
				auto id		=ord->GetID();
				auto sTime	=ord->GetCreatedTime();
				auto duration  =ord->GetLastestTime()-ord->GetCreatedTime();
				auto status =ord->GetOrderStatus();				
				auto side	=ord->GetDirection();
				auto px		=ord->GetPrice();
				auto size	=ord->GetSize();
				auto lvlCount = ord->GetCurrentLevel();

				auto statusCode=0;

				switch (status){
					case NEW:				statusCode = 0; break;
					case REPLACED:			statusCode = 1; break;
					case PARTIALLY_FILLED:	statusCode = 2; break;
					case FILLED:			statusCode = 3; break;
					case CANCELLED:			statusCode = 4; break;
				}
				for (auto j=0;j<8;j++){
					mwSize indicies[2] = { dataIdx,j };
					size_t index = mxCalcSingleSubscript(plhs[0], 2, indicies);
					double value=0;
					switch (j){
						case 0: value = id;			break;
						case 1: value = sTime;		break;
						case 2: value = duration;	break;
						case 3: value = side;		break;
						case 4: value = px;			break;
						case 5: value = size;		break;
						case 6: value = statusCode;	break;
						case 7: value = lvlCount;	break;
					}

					((double*)mxGetData(plhs[0]))[index] = value;
				}
			}

			nlhs = 1;
		}
	}
}
