#include "OrderBookEvent.h"

OrderBookEvent::OrderBookEvent(long long id, OrderEventType type, OrderEventReason rs, double px, double vol, int direction,double origTime, long pos) 
	: eventType(type), price(px), size(vol), direction(direction), id(id),reason(rs),originalTime(origTime){
	hitBookLevel=0;
}

OrderBookEvent::OrderBookEvent(OrderBookEvent & evt) {}
OrderBookEvent::OrderBookEvent(){}

void			OrderBookEvent::SetID(long long value)								{	id = value;			}	
void			OrderBookEvent::SetOrderEventType(OrderEventType type)				{	eventType = type;	}
void			OrderBookEvent::SetOrderEventReason(OrderEventReason value)			{	reason=value;		}	
void			OrderBookEvent::SetPrice(double value)								{	price = value;		}	
void			OrderBookEvent::SetSize(double value)								{	size = value;		}
void			OrderBookEvent::SetDirection(long value)							{	direction = value;	}
void			OrderBookEvent::SetOriginalEventTime(double value)					{	originalTime=value; }	
void			OrderBookEvent::SetHitBookLevel(long value)							{	hitBookLevel=value;	}

long long					OrderBookEvent::GetID() const							{	return id;			}
OrderEventType				OrderBookEvent::GetOrderEventType() const				{	return eventType;	}
OrderEventReason			OrderBookEvent::GetOrderEventReason() const				{	return reason;		}
double						OrderBookEvent::GetPrice() const						{	return price;		}
double						OrderBookEvent::GetSize() const							{	return size;		}
long						OrderBookEvent::GetDirection() const					{	return direction;	}	
double						OrderBookEvent::GetOriginalEventTime() const			{	return originalTime;}	
long						OrderBookEvent::GetHitBookLevel() const					{	return hitBookLevel;}		