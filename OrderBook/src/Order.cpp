#include "Order.h"

Order::Order() :direction(0), px(0), sz(0), id(0), isModified(false), createdTime(0), currentLvl(0){
}
Order::~Order(){
}

void Order::ProcessOrderBookEvent(const OrderBookEventPtr& value){
	id=value->GetID();
	auto type=value->GetOrderEventType();
	auto reason=value->GetOrderEventReason();

	auto eventPx=value->GetPrice();
	auto eventSz=value->GetSize();
	direction=value->GetDirection();

	events.push_back(value);

	lastestTime=value->GetOriginalEventTime();
	switch (reason){
		case OrderEventReason::Submission:
		{
			if (px!=0){				
				isModified=true;
			}else{
				createdTime=value->GetOriginalEventTime();				
			}
			modifiedTime=value->GetOriginalEventTime();
			px=eventPx;
			sz=eventSz;
			ordStatus=OrderStatus::NEW;
		}
		break;
		case OrderEventReason::Cancellation://Partially cancel some shares
		{
			sz=sz-eventSz;
			modifiedTime=value->GetOriginalEventTime();
			isModified=true;
			ordStatus=OrderStatus::REPLACED;
		}
		break;
		case OrderEventReason::Deletion:
		{
			ordStatus=OrderStatus::CANCELLED;
		}
		break;
		case OrderEventReason::Execution_Visible_Limit:
		{
			filledPx=(filledSz*filledPx+eventSz*eventPx)/(filledSz+eventSz);
			filledSz+=eventSz;
			if (filledSz<sz){
				ordStatus=OrderStatus::PARTIALLY_FILLED;
			}else{
				ordStatus=OrderStatus::FILLED;
			}
		}
		break;
		case OrderEventReason::Execution_Hidden_Limit:{
			std::cout<<"This thing shouldnt be here..."<<std::endl;
		}
		break;
		case OrderEventReason::TradingHalt:{
			ordStatus=OrderStatus::CANCELLED;
		}
		break;
	}
}

void Order::SetPrice(double value)				{px=value;}
void Order::SetFilledPrice(double value)		{filledPx=value;}
void Order::SetSize(double value)				{sz=value;}
void Order::SetFilledSize(double value)			{filledSz=value;}
void Order::SetID(long long value)				{id=value;}
void Order::SetDirection(long long value)		{direction=value;}
void Order::SetOrderStatus(OrderStatus value)	{ordStatus=value;}
void Order::SetCurrentLevel(int value)			{currentLvl = value; }

double		Order::GetPrice() const				{return px;}
double		Order::GetFilledPrice() const		{return filledPx;}
double		Order::GetSize() const				{return sz;}
double		Order::GetFilledSize() const		{return filledSz;}
long long	Order::GetID() const				{return id;}
long		Order::GetDirection() const			{return direction;}
OrderStatus Order::GetOrderStatus() const		{return ordStatus;}
int			Order::GetCurrentLevel() const		{ return currentLvl;}

double						Order::GetWorkingSize() const				{ return sz - filledSz; }
double						Order::GetCreatedTime() const				{return createdTime;}
double						Order::GetModifiedTime() const				{return modifiedTime;}
double						Order::GetLastestTime() const				{return lastestTime;}
bool						Order::IsModified() const					{return isModified;}

bool		Order::IsOpen() const				{return (bool) std::count (openSet, openSet+3, ordStatus);}