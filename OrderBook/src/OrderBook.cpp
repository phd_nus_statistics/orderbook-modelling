#include "OrderBook.h"
#include "EventLog.h"
#include "mex.h"
#include <ostream>

OrderBook::OrderBook(long minPrice, long maxPrice, std::vector<long> pxVec, long bookLevel) :isAutoMatchOrders(false), minPx(minPrice), maxPx(maxPrice), pxVec(pxVec), bookLevel(bookLevel){
	bidDepth.resize(maxPrice - minPrice + 1);
	askDepth.resize(maxPrice - minPrice + 1);
}
OrderBook::~OrderBook() {}
OrderBook::OrderBook(const OrderBook& ExistOrderBook) {}

void								OrderBook::SetAutoMatchOrders(bool value)	{isAutoMatchOrders=value;}
bool								OrderBook::IsAutoMatchOrders() const		{return isAutoMatchOrders;}
std::vector<OrderPtr>				OrderBook::GetOrders()						{return orderCollection;}
std::vector<OrderBookSnapshotPtr>	OrderBook::AddEvent(const OrderBookEventPtr& evt){
	auto id=evt->GetID();
	auto dir=evt->GetDirection();
	

	double marketOrderPx=0;	
	bool isMatchOrder=false;

	std::vector< std::vector<OrderPtr> > *depthPtr = (dir == 1) ? (&bidDepth) : (&askDepth);
	std::vector< std::vector<OrderPtr> > *contra_depthPtr = (dir == 1) ? (&askDepth) : (&bidDepth);

	for (int i = (dir == 1) ? (pxVec.size() - 1) : (0); (dir == 1) ? (i>=0) : (i<pxVec.size()); (dir == 1) ? (i--) : (i++)){
		if (contra_depthPtr->at(pxVec[i] - minPx).size()>0){
			marketOrderPx = pxVec[i];
			break;
		}		
	}

	//Checkk if within price
	if (evt->GetPrice()!=0 && (evt->GetPrice() < minPx || evt->GetPrice() >= maxPx)){
		return std::vector<OrderBookSnapshotPtr>();
	}

	if (evt->GetOrderEventType() != OrderEventType::Delete) {
		if (evt->GetOrderEventType() == OrderEventType::Add) {
			auto order = OrderPtr(new Order());
			activeOrderList.push_back(order);
			orderCollection.push_back(order);

			if (evt->GetPrice() == 0) {//Market order
				if (marketOrderPx != 0) {//If have opposite depth
					evt->SetPrice(marketOrderPx);
					isMatchOrder = true;
				}
				else {//Reject order
					return std::vector<OrderBookSnapshotPtr>();
				}
			}

			order->ProcessOrderBookEvent(evt);
			auto currentPrice = (int)(order->GetPrice() - minPx);
			(*depthPtr)[currentPrice].push_back(order);
			auto lvlCount = 1;
			for (auto i = (dir == 1) ? (depthPtr->size() - 1) : (0); (dir == 1) ? (i>0) : (i<depthPtr->size()); (dir == 1) ? (i--) : (i++)){
				if (depthPtr->at(i).size()>0){
					if (currentPrice == i){
						break;
					}
					lvlCount++;
				}
			}
			order->SetCurrentLevel(lvlCount);
			evt->SetHitBookLevel(lvlCount);
		}
		else {// Modify
			auto orderIter = std::find_if(activeOrderList.begin(), activeOrderList.end(), [evt](const OrderPtr& ord) {
				return ord->GetID() == evt->GetID();
			});
			if (orderIter != activeOrderList.end()) {
				(*orderIter)->ProcessOrderBookEvent(evt);
				evt->SetHitBookLevel((*orderIter)->GetCurrentLevel());
			}
		}
	}else{
		if (evt->GetID() == 0) {
			//Cannot find order to cancel. CAncel from the last of the queue
			auto currentPrice = (int)(evt->GetPrice() - minPx);
			if ((*depthPtr)[currentPrice].size() > 0){
				evt->SetID((*(*depthPtr)[currentPrice].rbegin())->GetID());
			}
		}
		auto findIter = std::find_if(activeOrderList.begin(), activeOrderList.end(), [evt](const OrderPtr& ord) {
			return ord->GetID()== evt->GetID();
		});

		if (findIter != activeOrderList.end()) {
			bool hasFound = false;
			auto ordVec=&(*depthPtr).at((long)(*findIter)->GetPrice() - minPx);
			for (auto orderIter = ordVec->begin(); orderIter != ordVec->end();) {
				if ((*orderIter)->GetID() == evt->GetID()) {
					(*orderIter)->ProcessOrderBookEvent(evt);
					evt->SetHitBookLevel((*orderIter)->GetCurrentLevel());
					orderIter = ordVec->erase(orderIter);
					hasFound = true;
					break;
				}else {
					orderIter++;
				}
			}
			activeOrderList.erase(findIter);
		}else {
			//Cannot find order to cancel
			return std::vector<OrderBookSnapshotPtr>();
		}
	}
	


	if (evt->GetOrderEventType() == OrderEventType::Add && isAutoMatchOrders) {
		if (isMatchOrder){
			std::vector<OrderPtr> *bestBidOrds = 0;
			std::vector<OrderPtr> *bestAskOrds = 0;

			for (auto i = pxVec.size() - 1; i > 0; i--){
				if (bidDepth.at(pxVec[i] - minPx).size() > 0){
					bestBidOrds = &(bidDepth.at(pxVec[i] - minPx));
					break;
				}
			}
			for (auto i = 0; i < pxVec.size(); i++){
				if (askDepth.at(pxVec[i] - minPx).size() > 0){
					bestAskOrds = &(askDepth.at(pxVec[i] - minPx));
					break;
				}
			}


			if (bestBidOrds != 0 && bestAskOrds != 0) {
				while (true){
					if (bestBidOrds->empty() || bestAskOrds->empty()) break;
					if (bestBidOrds->at(0)->GetPrice() >= bestAskOrds->at(0)->GetPrice()) {
						//Set last event to match
						evt->SetOrderEventReason(OrderEventReason::Execution_Visible_Limit);
						//Match order and generate events
						auto firstBidOrder = bestBidOrds->front();
						auto firstAskOrder = bestAskOrds->front();

						//Generate events
						auto execSize = std::min(firstBidOrder->GetSize(), firstAskOrder->GetSize());
						auto matchPx = firstBidOrder->GetPrice();
						auto matchOrder = [matchPx, execSize, evt](OrderPtr& orgOrder)->OrderBookEventPtr {
							OrderBookEventPtr generatedEvent;
							if (orgOrder->GetWorkingSize() > execSize) {
								orgOrder->SetOrderStatus(OrderStatus::PARTIALLY_FILLED);
							}
							else {
								orgOrder->SetOrderStatus(OrderStatus::FILLED);
								generatedEvent.reset(new OrderBookEvent(orgOrder->GetID(), OrderEventType::Delete, OrderEventReason::Execution_Visible_Limit, matchPx, matchPx, orgOrder->GetDirection(), evt->GetOriginalEventTime()));
							}
							orgOrder->SetFilledPrice(matchPx);
							orgOrder->SetFilledSize(orgOrder->GetFilledSize() + execSize);

							return generatedEvent;
						};
						auto generatedBidEvent = matchOrder(firstBidOrder);
						auto generatedAskEvent = matchOrder(firstAskOrder);
						auto removeOrder = [this](boost::shared_ptr<Order>& delOrder, std::vector<OrderPtr> * orderQueue, std::vector< std::vector<OrderPtr> > *depthPtr){
							if (delOrder->GetOrderStatus() == OrderStatus::FILLED) {
								auto findIter = std::find_if(activeOrderList.begin(), activeOrderList.end(), [delOrder](const OrderPtr& ord) {
									return ord->GetID() == delOrder->GetID();
								});
								if (findIter != activeOrderList.end()) {
									activeOrderList.erase(findIter);
								}
								orderQueue->erase(orderQueue->begin());
							}
						};
						removeOrder(firstBidOrder, bestBidOrds, &bidDepth);
						removeOrder(firstAskOrder, bestAskOrds, &askDepth);
					}
					else break;
				}
			}
		}
	}

	lastEvent = evt;

	std::vector<OrderBookSnapshotPtr> snapshots;
	snapshots.push_back(OrderBookSnapshotPtr(new OrderBookSnapshot(this)));
	return snapshots;
}