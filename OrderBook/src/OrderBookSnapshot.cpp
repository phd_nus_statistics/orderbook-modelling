#include "OrderBookSnapshot.h"
#include "OrderBook.h"
#include <iostream>

std::vector<SnapshotData> GetData(const std::vector< std::vector<OrderPtr> >& depth, bool isBid, OrderBookEventPtr lastEvent, const std::vector<long>& pxVec, long minPx, long bookLevel){
	std::vector<SnapshotData> snapshotDataVector;
	//Get statistics for each price
	bool foundLastEvent = false;
	
	for (int i = (isBid) ? (pxVec.size() - 1) : (0); (isBid) ? (i >= 0) : (i<pxVec.size()); (isBid) ? (i--) : (i++)){
		auto depthIdx = pxVec[i] - minPx;
		if (depth[depthIdx].size() > 0) {
			SnapshotData data;

			data.px = depth[depthIdx].back()->GetPrice();

			for (auto &ord : depth[depthIdx]){
				data.sz += ord->GetWorkingSize();

			}
			if (data.px == lastEvent->GetPrice()) {
				foundLastEvent = true;
				data.lastEvent = lastEvent;
			}
			snapshotDataVector.push_back(data);

			if (snapshotDataVector.size() >= bookLevel) break;
		}
	}
	return snapshotDataVector;
};
OrderBookSnapshot::OrderBookSnapshot(const OrderBook* ordBook) :
isLastTradeBid(false), isLastTradeAsk(false), snapshotTime(0){
	//CONSTRUCT THE MARKET DEPTH	
	OrderBookEventPtr lastEvent=ordBook->lastEvent;
	
	snapshotData_BID = GetData(ordBook->bidDepth, true, lastEvent, ordBook->pxVec, ordBook->minPx, ordBook->bookLevel);
	snapshotData_ASK = GetData(ordBook->askDepth, false, lastEvent, ordBook->pxVec, ordBook->minPx, ordBook->bookLevel);
	snapshotTime=ordBook->lastEvent->GetOriginalEventTime();
	if (ordBook->lastEvent->GetOrderEventReason()==OrderEventReason::Execution_Visible_Limit){
		if (ordBook->lastEvent->GetDirection()==1){
			isLastTradeBid=true;
		}else{
			isLastTradeAsk=true;
		}
	}else{
		isLastTradeBid=false;
		isLastTradeAsk=false;
	}	
}