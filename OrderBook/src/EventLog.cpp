#include "EventLog.h"
#include "boost/date_time/posix_time/posix_time.hpp"
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include "boost/iostreams/stream.hpp"
#include "boost/iostreams/device/null.hpp"
namespace fs = boost::filesystem;
std::ofstream*	Logger::filePtr		=0;
TeeStream*		Logger::streamPtr	=0;
bool			Logger::consoleOuput=true;
NullStream*		Logger::nullStreamPtr=0;
void			Logger::SetPath(std::string Path){
	try{
		//Create dir if not exists
		fs::path data_dir(Path);
		if (!fs::exists( data_dir ))  
			boost::filesystem::create_directory(data_dir);
		boost::posix_time::ptime initTime=boost::posix_time::second_clock::local_time();
		std::stringstream ss;
		ss<<initTime.date().day().as_number()<<"."<<initTime.date().month().as_number()<<"."<<initTime.date().year()<<"."<<initTime.time_of_day().total_seconds()<<".txt";
		Path+="/"+ss.str();
        filePtr			=new  std::ofstream(Path.c_str());
		if (consoleOuput){
			Tee* teePtr = new Tee(std::cout, *filePtr);
			streamPtr		=new TeeStream(*teePtr);
		}else{
			nullStreamPtr=new boost::iostreams::stream<boost::iostreams::null_sink>{ boost::iostreams::null_sink{} };
			Tee* teePtr = new Tee(*nullStreamPtr, *filePtr);
			streamPtr = new TeeStream(*teePtr);
		}		
	} catch (std::exception e){
		std::cout<<"Logger Error: "<<e.what()<<std::endl;
	}
}
void			Logger::SetConsoleLogging(bool value){
	consoleOuput=value;
}
TeeStream&		Logger::Log(LOG_LEVEL level){
	if (streamPtr!=0){
		std::ostringstream msg;
		const boost::posix_time::ptime now=	boost::posix_time::second_clock::local_time();
		boost::posix_time::time_facet*const f=new boost::posix_time::time_facet("%H-%M-%S");
		msg.imbue(std::locale(msg.getloc(),f));
		msg << now;
		try{
			switch (level){
				case LOG_INFO:
					*streamPtr<<msg.str()<<" INFO: ";
					break;
				case LOG_WARN:
					*streamPtr<<msg.str()<<" WARN: ";
					break;
				case LOG_FATAL:
					*streamPtr<<msg.str()<<" FATAL: ";
					break;
				case LOG_DEBUG:							
					*streamPtr<<msg.str()<<" DEBUG: ";
					break;
				default:
					*streamPtr<<msg.str()<<"INFO: ";
					break;
			}
			return *streamPtr;
		} catch (const std::exception& e){
			std::cout<<"Logger Error: "<<e.what()<<std::endl;
		}
	}
	return *streamPtr;	
};
std::ofstream&	Logger::LogToFile(LOG_LEVEL level){
	if (filePtr!=0){
		std::ostringstream msg;
		const boost::posix_time::ptime now=	boost::posix_time::second_clock::local_time();
		boost::posix_time::time_facet*const f=new boost::posix_time::time_facet("%H-%M-%S");
		msg.imbue(std::locale(msg.getloc(),f));
		msg << now;
		try{
			switch (level){
				case LOG_INFO:
					*filePtr<<msg.str()<<" INFO: ";
					break;
				case LOG_WARN:
					*filePtr<<msg.str()<<" WARN: ";
					break;
				case LOG_FATAL:
					*filePtr<<msg.str()<<" FATAL: ";
					break;
				case LOG_DEBUG:							
					*filePtr<<msg.str()<<" DEBUG: ";
					break;
				default:
					*filePtr<<msg.str()<<"INFO: ";
					break;
			}
			return *filePtr;
		} catch (const std::exception& e){
			std::cout<<"Logger Error: "<<e.what()<<std::endl;
		}
	}
	return *filePtr;	
}
