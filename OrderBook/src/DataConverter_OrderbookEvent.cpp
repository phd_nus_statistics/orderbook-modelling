#include "DataConverter_OrderbookEvent.h"
#include <stdio.h>

DataConverter_OrderbookEvent::DataConverter_OrderbookEvent(){

}
DataConverter_OrderbookEvent::~DataConverter_OrderbookEvent(){

}
std::vector<OrderBookEventPtr> DataConverter_OrderbookEvent::GetOrderBookEvents() const{
	return orderbookEvents;
}
void DataConverter_OrderbookEvent::ProcessFile(const std::string& filePath, bool isReset){
	OnProcessFile(filePath);
	if (isReset) orderbookEvents.clear();

	FILE* file = fopen(filePath.c_str(), "r");
	char line[4096];
	long lineCount=0;
	while (fgets(line, sizeof(line), file)) {
		OnNewEventRaw(line,strlen(line)-1);
    }
    fclose(file);
	
}