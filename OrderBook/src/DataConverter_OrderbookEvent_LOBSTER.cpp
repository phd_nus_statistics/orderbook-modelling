
#include "DataConverter_OrderbookEvent_LOBSTER.h"
#include <boost/algorithm/string.hpp>
#include "mex.h"
DataConverter_OrderbookEvent_LOBSTER::DataConverter_OrderbookEvent_LOBSTER(long tickSz) :tickSize(tickSz){

}
DataConverter_OrderbookEvent_LOBSTER::~DataConverter_OrderbookEvent_LOBSTER(){

}
void DataConverter_OrderbookEvent_LOBSTER::OnProcessFile(const std::string& filePath){
	//Get new date from LOBSTER file
	std::vector<std::string> strs;
	boost::split(strs,filePath,boost::is_any_of("\\"));
	auto fileName=strs.back();
	boost::split(strs,fileName,boost::is_any_of("_"));
	auto dtStr=strs[1];
	boost::split(strs,dtStr,boost::is_any_of("-"));
	int year	=std::stoi(strs[0].c_str());
	int month	=std::stoi(strs[1].c_str());
	int day		=std::stoi(strs[2].c_str());
	startTime=boost::posix_time::ptime(boost::gregorian::date(year,month,day),boost::posix_time::time_duration(0,0,0));
	originalTime=0;
}
void DataConverter_OrderbookEvent_LOBSTER::OnNewEventRaw(char* data, int len){
	OrderEventType evtType=OrderEventType::Add;
	OrderEventReason evtReason= OrderEventReason::Cancellation;
	long long orderID=0;
	long long orderSize=0;
	bool isBuyOrder=0;

	char* token = strtok(data, ",");			
	int tokenIdx=0;
	while (token) {
		switch (tokenIdx){
			case 0:
			{
				boost::split(timeSplitStr,token,boost::is_any_of("."));				
				double secs		=std::stoll(timeSplitStr[0].c_str());
				double secRatio	=0;
				if (timeSplitStr.size()==2)  secRatio=std::stoll(timeSplitStr[1].c_str());
				evtTime=startTime+boost::posix_time::time_duration(0,0,secs,secRatio/1000000000.0);
				originalTime=std::stof(token);
			}
			break;
			case 1:
			{
				int type = std::stoi(token);
				switch (type){
					case 1:
						evtType=OrderEventType::Add;
						evtReason=OrderEventReason::Submission;
					break;
					case 2:
						evtType=OrderEventType::Change;
						evtReason=OrderEventReason::Cancellation;
					break;
					case 3:
						evtType=OrderEventType::Delete;
						evtReason=OrderEventReason::Deletion;
					break;
					case 4:
						evtType=OrderEventType::Delete;
						evtReason=OrderEventReason::Execution_Visible_Limit;
					break;
					case 5:
						evtType=OrderEventType::Delete;
						evtReason=OrderEventReason::Execution_Hidden_Limit;
					break;
					case 6:
						evtType = OrderEventType::Delete;
						evtReason = OrderEventReason::Execution_Hidden_Limit;
					break;
					case 7:
						evtType=OrderEventType::Delete;
						evtReason=OrderEventReason::TradingHalt;
					break;
				}
				break;
			}
			case 2:
				orderID=std::stoll(token);
			break;
			case 3:
				orderSize=std::stoll(token);
			break;
			case 4:
				orderPx = std::stoll(token) / tickSize;
			break;
			case 5:
				direction=std::stoi(token);
			break;
		}
		token = strtok(NULL, ",");
		tokenIdx++;
	}	
	orderbookEvents.push_back(OrderBookEventPtr(new OrderBookEvent(orderID,evtType,evtReason,orderPx,orderSize,direction,originalTime)));
}