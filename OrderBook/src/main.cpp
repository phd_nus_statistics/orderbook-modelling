#include <string>
#include <sstream>
#include "boost/date_time.hpp"
#include "DataConverter_OrderbookEvent_LOBSTER.h"
#include "OrderBook.h"
#include "OrderBookSnapshot.h"
#include "EventLog.h"

int main(int argc, char *argv[]){
	std::vector<std::string> arguments(argv + 1, argv + argc);
	auto fileName = arguments[1];
	DataConverter_OrderbookEvent_LOBSTER dataConverter;

	dataConverter.ProcessFile(fileName);
	auto events = dataConverter.GetOrderBookEvents();

	long minPx = 9999999999999;
	long maxPx = 0;
	for (auto evtIdx = 0; evtIdx<events.size(); evtIdx++) {
		minPx = std::min(minPx, (long)events[evtIdx]->GetPrice());
		maxPx = std::max(maxPx, (long)events[evtIdx]->GetPrice());
	}
	OrderBookPtr orderbook(new OrderBook(minPx, maxPx));
	orderbook->SetAutoMatchOrders(0);


	std::vector<OrderBookSnapshotPtr> snapshots;
	for (auto evtIdx = 0; evtIdx<events.size(); evtIdx++) {
		auto generatedSnapshots = orderbook->AddEvent(events[evtIdx]);
		snapshots.insert(snapshots.end(), generatedSnapshots.begin(), generatedSnapshots.end());
	}
}